data {
    int<lower=0> n1;
    int<lower=0> n2;
    real x1[n1];
    real x2[n2];
    real mu_prior_mu;
    real mu_prior_sigma;
    real<lower=0> sigma_prior_low;
    real<lower=0> sigma_prior_high;
}

parameters {
    real<lower=1., upper=135> nu;
    real mu1;
    real mu2;
    real<lower=sigma_prior_low, upper=sigma_prior_high> sigma1;
    real<lower=sigma_prior_low, upper=sigma_prior_high> sigma2;
}

model {
    nu - 1 ~ exponential(1. / 29.);
    mu1 ~ normal(mu_prior_mu, mu_prior_sigma);
    mu2 ~ normal(mu_prior_mu, mu_prior_sigma);
    x1 ~ student_t(nu, mu1, sigma1);
    x2 ~ student_t(nu, mu2, sigma2);
}

generated quantities {
    real mu1_minus_mu2 = mu1 - mu2;
    real sigma1_minus_sigma2 = sigma1 - sigma2;
    real log10_nu = log10(nu);
}
